* -*- mode: sysrpl -*-
*
* TIMEDONE example from page 41 of the book
*
*   An Introduction to HP48 System RPL and Assembly Language
*   Programming by James Donnelly.
*
* TIMEDONE ( -> $ )
*
::
  0LASTOWDOB!
  CK0NOLASTWD
  "Program complete"
  FORTY SysITE
    ::
      " at "
      TOD TOD>t$ &$
    ;
    "."
  &$
;
