;;; -*- mode: emacs-lisp; lexical-binding: t -*-

;;; sasm-mode.el -- Major mode for Saturn assembly language

;; Copyright (C) 2015 - 2018 Paul Onions

;; Author: Paul Onions <paul.onions@acm.org>
;; Keywords: Saturn, HP48, HP49, HP50, calculator

;; This file is free software, see the LICENCE file in this directory
;; for copying terms.

;;; Commentary:

;; A major mode for Saturn assembly language, the processor (perhaps
;; emulated) in HP48/49/50-series calculators.

;;; Code:
(require 'cl-lib)
(require 'rpl-base)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Customizations
;;
(defcustom sasm-assembler-program "sasm"
  "External SASM assembler program name."
  :type 'string
  :group 'rpl)

(defcustom sasm-assembler-listing-bufname "*sasm-listing*"
  "Buffer name in which to capture SASM assembler listing."
  :type 'string
  :group 'rpl)

(defface sasm-label '((t :inherit font-lock-constant-face))
  "Face used for displaying SASM labels."
  :group 'rpl)

(defface sasm-mnemonic '((t :inherit font-lock-keyword-face))
  "Face used for displaying SASM mnemonics."
  :group 'rpl)

(defface sasm-comment '((t :inherit font-lock-comment-face))
  "Face used for displaying SASM comments."
  :group 'rpl)

(defcustom sasm-font-lock-label-face 'sasm-label
  "Name of face to use for displaying SASM labels."
  :type 'symbol
  :group 'rpl)

(defcustom sasm-font-lock-keyword-face 'sasm-mnemonic
  "Name of face to use for displaying SASM mnemonics."
  :type 'symbol
  :group 'rpl)

(defcustom sasm-font-lock-comment-face 'sasm-comment
  "Name of face to use for displaying SASM comments."
  :type 'symbol
  :group 'rpl)

(defvar sasm-mode-syntax-table
  (let ((table (make-syntax-table prog-mode-syntax-table)))
    (modify-syntax-entry ??  "w" table)
    (modify-syntax-entry ?#  "w" table)
    (modify-syntax-entry ?=  "w" table)
    (modify-syntax-entry ?<  "w" table)
    (modify-syntax-entry ?>  "w" table)
    (modify-syntax-entry ?+  "w" table)
    (modify-syntax-entry ?-  "w" table)
    (modify-syntax-entry ?!  "w" table)
    (modify-syntax-entry ?&  "w" table)
    (modify-syntax-entry ?\( "w" table)
    (modify-syntax-entry ?\) "w" table)
    table)
  "The SASM syntax table.")

(defvar sasm-font-lock-keywords
  (list (list "^\\*.*$" (list 0 'sasm-font-lock-comment-face))
        ;; !!! TODO !!!
        ))

(defun sasm-compile-buffer ()
  "Assemble the current buffer."
  (interactive)
  (let* ((src-filename (make-temp-file "sasm" nil ".a"))
	 (obj-filename (concat (file-name-sans-extension src-filename) ".o"))
         (rtn-code 0))
    (write-region (point-min) (point-max) src-filename)
    (with-current-buffer (get-buffer-create sasm-assembler-listing-bufname)
      (setq buffer-read-only nil)
      (erase-buffer)
      (setq rtn-code (call-process sasm-assembler-program nil t nil
				   "-A" "-o" obj-filename src-filename))
      (fundamental-mode)
      (beginning-of-buffer)
      (setq buffer-read-only t))
    (let ((win (display-buffer sasm-assembler-listing-bufname)))
      (when sysrpl-select-popup-windows
	(select-window win)))
    (if (eql rtn-code 0)
	(message "Assembly complete")
      (message "*** Assembled with ERRORS ***"))))

(defun sasm-get-eldoc-message ()
  (interactive)
  ;; !!! TODO !!!
  "")

(defvar sasm-mode-map
  (let ((map (make-sparse-keymap))
        (menu-map (make-sparse-keymap)))
    (set-keymap-parent map rpl-common-keymap)
    ;; Menu items
    (define-key map [menu-bar rpl-menu] (cons "RPL" menu-map))
    (define-key menu-map [sasm-menu-separator-1]
      '(menu-item "--"))
    ;; !!! TODO !!!
    map)
  "The SASM mode local keymap.")

(defvar sasm-mode-hook nil
  "Hook for customizing SASM mode.")

(define-derived-mode sasm-mode asm-mode "SASM"
  "Major mode for SASM assembler language."
  :group 'rpl
  (make-local-variable 'eldoc-documentation-function)
  (setq eldoc-documentation-function 'sasm-get-eldoc-message)
  (setq font-lock-defaults (list 'sasm-font-lock-keywords))
  (setq rpl-menu-compile-buffer-enable t))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; End of file
;;
(provide 'sasm-mode)
