;;; -*- mode: emacs-lisp; lexical-binding: t -*-

;;; rpl-tools.el -- tools for RPL calculator programming

;; Copyright (C) 2014 Paul Onions

;; Author: Paul Onions <paul.onions@acm.org>
;; Keywords: RPL, SysRPL, HP48, HP49, HP50, calculator

;; This file is free software, see the LICENCE file in this directory
;; for copying terms.

;;; Commentary:

;; To use this system, ensure this directory is in your `load-path',
;; and put
;;
;;   (require 'rpl-tools)
;;
;; into your .emacs file.
;;
;; This will provide a major mode called `sysrpl-mode' to be used when
;; editing System RPL program files.  This mode will highlight valid
;; System RPL command names and also display information about them
;; via the `sysrpl-apropos-thing-at-point' command.  It is also
;; `eldoc' capable, so when `eldoc-mode' is enabled a stack effect
;; string for the SysRPL word at point is displayed in the minibuffer.
;;
;; Also work-in-progress is `sasm-mode' for Saturn assembler buffers.
;;
;; See also the ``RPL'' menu added to all `sysrpl-mode' and
;; `sasm-mode' buffers.

;;; Code:

;; Load everything
(require 'rpl-base)
(require 'rpl-edb)
(require 'sysrpl-mode)
(require 'sasm-mode)

;; Acknowledge we're loaded
(provide 'rpl-tools)
